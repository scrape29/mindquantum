mindquantum.algorithm.mapping
======================================

.. py:module:: mindquantum.algorithm.mapping


MindQuantum 比特映射模块。

.. mscnautosummary::
    :toctree:
    :nosignatures:
    :template: classtemplate.rst

    mindquantum.algorithm.fold_at_random
    mindquantum.algorithm.zne
